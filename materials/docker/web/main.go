package main

import (
	"fmt"
	"html/template"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/shirou/gopsutil/v3/mem"
)

type Content struct {
	IP        string
	Count     int
	Memory_MB float64
	Memory_GB float64
	Number    int
}

var RedisEndpoint = "redis-service:6379" // Pod can connect by service-name in the same namespace.

var CLIENT = RedisClient()

func RedisClient() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     RedisEndpoint,
		Password: "",
		DB:       0,
	})
	pong, err := client.Ping().Result()
	if err != nil {
		fmt.Println(pong, err)
	} else {
		fmt.Println("Connect Redis Success!")
	}

	return client
}

func countIP(w http.ResponseWriter, r *http.Request) {
	client_ip, _, _ := net.SplitHostPort(r.RemoteAddr)

	var counting int

	counting, err := CLIENT.Get(client_ip).Int()
	if err == redis.Nil {
		counting = 1
		CLIENT.Set(client_ip, counting, 60*time.Second)
	} else {
		ttl, _ := CLIENT.TTL(client_ip).Result()
		counting++
		CLIENT.Set(client_ip, counting, ttl)

		if ttl < time.Duration(0) {
			CLIENT.Del(client_ip)
		}
	}

	// show the result in HTML
	tmpl := template.Must(template.ParseFiles("./index.html"))

	data := Content{
		IP:    client_ip,
		Count: counting,
	}
	tmpl.Execute(w, data)
}

func memoryBound(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("./memory.html"))
	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}
	number := r.FormValue("number")
	num, _ := strconv.Atoi(number)
	var arr [][]int

	pow := 2

	var used_MB float64
	var used_GB float64
	v, _ := mem.VirtualMemory()
	last_memory := v.Available
	for i := 0; i < num; i++ {
		var tmp []int
		for j := 0; j < pow; j++ {
			tmp = append(tmp, j)
		}
		arr = append(arr, tmp)
		pow *= 2

		// check memory
		v, _ := mem.VirtualMemory()
		current_memory := v.Available
		used_memory, _ := strconv.ParseFloat(fmt.Sprintf("%.4f", float64(last_memory-current_memory)), 64)
		used_MB, _ = strconv.ParseFloat(fmt.Sprintf("%.4f", float64(used_memory/float64(1024)/float64(1024))), 64)
		used_GB, _ = strconv.ParseFloat(fmt.Sprintf("%.4f", float64(used_MB/float64(1024))), 64)
		fmt.Printf("%d: %f MB, %f GB\n", i, used_MB, used_GB)
		last_memory = current_memory
	}
	// show the result in HTML
	data := Content{
		Number:    num,
		Memory_MB: used_MB,
		Memory_GB: used_GB,
	}
	tmpl.Execute(w, data)
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("hello world")
}

func main() {
	http.HandleFunc("/", hello)
	http.HandleFunc("/home", countIP)
	http.HandleFunc("/memory", memoryBound)

	if err := http.ListenAndServe(":8000", nil); err != nil {
		fmt.Println(err)
	}
}
