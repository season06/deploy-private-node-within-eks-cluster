export AWS_REGION=us-east-1
export EKS_CLUSTER=eks-cluster
export VPC_NAME=EKS-VPC
export PUBLIC_SUBNET_1_NAME=EKS-PublicSubnet01
export PUBLIC_SUBNET_2_NAME=EKS-PublicSubnet02
export PRIVATE_SUBNET_1_NAME=EKS-PrivateSubnet01
export PRIVATE_SUBNET_2_NAME=EKS-PrivateSubnet02

# export ACCOUNT=$(aws sts get-caller-identity --query Account --output text)
# export VPC_ID=$(aws ec2 describe-vpcs --filter Name=tag:Name,Values=$VPC_NAME --query Vpcs[].VpcId --output text)
# export PUBLIC_SUBNET_1_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PUBLIC_SUBNET_1_NAME --query 'Subnets[?MapPublicIpOnLaunch==`true`].SubnetId' --output text)
# export PUBLIC_SUBNET_2_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PUBLIC_SUBNET_2_NAME --query 'Subnets[?MapPublicIpOnLaunch==`true`].SubnetId' --output text)
# export PRIVATE_SUBNET_1_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PRIVATE_SUBNET_1_NAME --query 'Subnets[?MapPublicIpOnLaunch==`false`].SubnetId' --output text)
# export PRIVATE_SUBNET_2_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PRIVATE_SUBNET_2_NAME --query 'Subnets[?MapPublicIpOnLaunch==`false`].SubnetId' --output text)

cat << EOF > cluster.yaml
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig
metadata:
  name: $EKS_CLUSTER
  region: $AWS_REGION

vpc:
  id: $VPC_ID
  cidr: "192.168.0.0/16"
  subnets:
    public:
      us-east-1a:
        id: $PUBLIC_SUBNET_1_ID
      us-east-1b:
        id: $PUBLIC_SUBNET_2_ID
    private:
      us-east-1a:
        id: $PRIVATE_SUBNET_1_ID
      us-east-1b:
        id: $PRIVATE_SUBNET_2_ID

nodeGroups:
  - name: EKS-public-workers
    instanceType: t2.medium
    desiredCapacity: 1
  - name: EKS-private-workers
    instanceType: t2.medium
    desiredCapacity: 2
    privateNetworking: true
EOF

eksctl create cluster -f cluster.yaml