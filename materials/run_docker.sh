export AWS_REGION=us-east-1
export ACCOUNT=$(aws sts get-caller-identity --query Account --output text)
export DOCKER_REGISTRY=$ACCOUNT.dkr.ecr.$AWS_REGION.amazonaws.com
export REDIS_REPO=redis-repository
export WEB_REPO=web-repository

aws ecr create-repository --repository-name $REDIS_REPO
aws ecr create-repository --repository-name $WEB_REPO

cat << EOF > docker-compose.yml
version: "3.3"
services:
  redis:
    image: $DOCKER_REGISTRY/redis-repository 
    build: ./docker/redis
    ports:
      - "6379:6379"
  golang_web:
    image: $DOCKER_REGISTRY/web-repository 
    build: ./docker/web
    ports:
      - "8000:8000"
EOF

docker-compose build
docker-compose push