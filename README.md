# Deploy private node within EKS Cluster

## Introduction

### What is Amazon Elastic Kubernetes Service?
Amazon Elastic Kubernetes Service (Amazon EKS) is a managed service that you can use to run Kubernetes on AWS **without needing to install, operate, and maintain** your own **Kubernetes control plane or nodes**. Kubernetes is an open-source system for automating the deployment, scaling, and management of containerized applications. <br>
<br>
EKS integrated with many AWS services to provide scalability and security for your applications, including the following capabilities:
- ECR
- Elastic Load Balancing
- IAM for authentication
- VPC

### Kubernetes Architecture
<img src="./images/k8s-architecture.png" width="70%" height="70%">

#### ⭐ Pod
- The **smallest unit** of Kubernetes operation.
- There can be **one or more Containers** in a Pod.
- Containers in the same Pod share the same resources and network, and communicate through **local ports**.

#### ⭐ Worker Node
Worker Node is like a **computer engine**. In AWS, the worker node is an EC2 Instance or Fargate. <br>
Every Worker Node has three components:
1. kubelet
    - **Communicating** with Master Node.
    - **Managing** the status of Pods on this Node. 
2. kube-proxy
    - Related to `Service`, we will talk about this later.
    - It is responsible for the **connection between Service and Pod**.
    - Provide **Service Discovery** and **Load Balance** inside Node for Service.
3. container runtime
    - It is the program that **executes the container**.

#### ⭐ Master Node (Control Plane)
Master Node like an administrator in Kubernetes. <br>
Every Master Node has three components:
1. API server
    - The API interface of Kubernetes.
    - Every command, like `kubectl`, will go through the API Server to each Pod.
2. Controller Manager
    - The program that continues to operate in the background and monitors the process of the cluster through the API Server.
    - Controller Manager will **restore the system state** to "desired state" defined at the beginning when any accident or failure occurs
    - Controller Manager contains many controller types: <br>
        <img src="./images/controller_manager.png" width="50%" height="50%">
3. Scheduler
    - The scheduler for pods.
    - When a **new pod is created**, Scheduler will **find the most suitable Node** according to the resource configuration of Node and deploy the pod to it.
4. etcd
    - **Backup cluster data**. When master node fails, etcd can use backup data to restore the system state.

#### ⭐ Cluster
A collection of multiple **Master Node** and **Worker Node** in Kubernetes.

### Main Components
#### ⭐ Deployment
Deployment is one of a Workloads in Kubernetes.
> What is Workloads? <br>
> A workload is an **application** running on Kubernetes. Whether your workload is a single component or several that work together, on Kubernetes you run it inside a set of pods.

Deployment can help us achieve the following things:
1. Deploy a application.
2. **Auto Scaling** Pods.
3. **Upgrade** or **Rollback** applications to a specific version.
4. **Zero downtime deployment** during upgrade or rollback application.

❓ How to write the deployment.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: my-pod
  template:
    metadata:
      labels:
        app: my-pod
    spec:
      containers:
      - name: my-container
        image: {Your_AWS_Account}.dkr.ecr.{Region}.amazonaws.com/my-repository:tag
        ports:
        - containerPort: 1234
```
- `apiVersion` <br>
    `apps/v1` is for kubectl versions >= 1.9.0
- `metadata.name` <br>
    The name of this Deployment.
- `spec` (define the configuration of **deployment**)
    - `replicas` <br>
        The number of pods to scale.
    - `selector` <br>
        Specify the condition of the Pod we selected. (labels)
    - `template` (define the configuration of **pod**)
        - `metadata.labels` <br>
            The labels of this pod. This `metadata.labels` must be included in `spec.selector`.
        - `spec` (define the configuration of **container**) <br>
            - `containers.name` <br>
                The name of this container.
            - `containers.image` <br>
                The image source of this container. It can be Dockerhub or ECR.
            - `containers.ports` <br>
                Which port of container allows external resource access.

❗ Use command to operate Deployment.
- Create a Deployment
    ```
    kubectl apply -f <deployment.yaml>
    ```
- Get all of pods
    ```
    kubectl get pods
    ```
- Get the information of deployments in Kubernetes
    ```
    kubectl get deployments
    ```
- Change the specific image version for pod
    ```
    kubectl set image deploy/<deployment-name> <container-name>=<image-path>:<tag>
    ```
- Check the history of image version of the deployment 
    ```
    kubectl rollout history deploy <deployment-name>
    ```

#### ⭐ Service
💭 Why we need Service? <br>
The life cycle of a pod is **dynamic**. When the pod is restarted, IP will change. So Service provides a **domain name**, allow external resource to access pod. The service is responsible for the **conversion** between domain name and IP.

- There are four **Service Type** to define different connection method.
    1. `ClusterIP` <br>
        **Internal access** in the cluster
    2. `NodePort` <br>
        **External access** through port mapping **`nodeIP:nodePort`**.
    3. `LoadBalancer` <br>
        Like NodePort. But it will also apply for a **Load Balancer** from your **cloud provider**.
    4. `ExternalName` <br>
        Mapping the service to a DNS Name instead of a label selector.

❓ How to write the service.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app: my-pod
  ports:
  - port: 80
    protocol: TCP
    targetPort: 1234
```
- `metadata.name` <br>
    - The name of this Service
- `spec` (define the configuration of Service)
    - `type` <br>
        Define the service type. Default is `ClusterIP`.
    - `selector` <br>
        Specify the condition of the Pod we selected. (labels)
    - `ports`
        - `port` <br>
            The port of **Service**.
        - `nodePort` <br>
            When the service type is **`NodePort`**, define which **Node** port mapping to targetPort. Default is a random port (range: 30000~32767).
        - `targetPort` <br>
            Define a expose port of **Pod**. Default is same as `spec.ports.port`.
        - `protocol` <br>
            TCP / UDP. Default is `TCP`.

❗ Use command to operate Service.
- Create a Service
    ```
    kubectl apply -f <service.yaml>
    ```
- Get all of Services
    ```
    kubectl get service
    ```
- Get detailed information of Service
    ```
    kubectl describe service <service-name>
    ```

❓ After created Service, how to associate application?
- After Service is created, the Service will be assigned a **virtual ip** (Cluster IP), and connect to the Endpoint which build from Pod. 
    - According to the above .yaml, there will generate the following network:
        > Pod <---> Endpoint(tcp:1234) <---> Service(tcp:80, with virtual ip)
- In Kubernetes cluster, there will be a DNS server to handle the mapping between domain name and ip. So that we can use **service name** as a domain name to access the application.
    - my-service
    - my-service.my-namespace (in the different namespace)

#### ⭐ Ingress
Ingress can manage external access to the services in a cluster. Using Ingress can implement Load Balancer, and expose the single port of Node for external traffic to access.

<img src="./images/ingress_intro.png" width="50%" height="50%">

❓ How to write the ingress.yaml
> There are more [Ingress annotations](https://kubernetes-sigs.github.io/aws-load-balancer-controller/guide/ingress/annotations/) for AWS Load Balancer.
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
  annotations:
    kubernetes.io/ingress.class: alb
    alb.ingress.kubernetes.io/scheme: internet-facing
    alb.ingress.kubernetes.io/target-type: instance
spec:
  rules:
    - http:
        paths:
        - path: /
          backend:
            serviceName: my-service
            servicePort: 1234
```
- `metadata`
    - `name` <br>
        The name of this Ingress
    - `annotations` <br>
        Define the parameter for AWS Load Balancer.
- `spec.rules.http.paths`
    - `path` <br>
        Forward traffic based on path.
    - `backend` <br>
        Which service to be forwarded.

---
## Scenario
Control Plane is managed by AWS, we only need to build the customize VPC below. <br>
This architecture has **two instances** in the **private subnet**. Every Node has several pods created by **Deployment**. The traffic can only enter from ALB which create from **Ingress**. And use **Service** to import traffic to each node according to the url path.

## Overview
<img src="./images/eks-architecture.png" width="70%" height="70%">

#### How will we achieve this?
1. Create a VPC environment.
2. Create ECR repository, build dockerfile into Image, and push it to ECR.
3. Setup IAM let EC2 have permission to create EKS Cluster.
4. Create EKS Cluster.
5. Apply Deployment to generate pod.
6. Apply Service and Ingress to implement load balance.

## Prerequisites
- Basic understanding of AWS **VPC**, **ALB**, and **Container** knowledge.
- Make sure the region is **US East (N. Virginia)**.
- Download the source file and project:
    - [vpc.yaml](./prerequisites/eks-vpc-cloudformation.yaml)
    - [materials](./materials)

## Step by Step
### Step 1 : Setup VPC network
- Go to [CloudFormation](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/)
- Use **CloudFormation** template ([eks-vpc-cloudformation.yaml](./prerequisites/eks-vpc-cloudformation.yaml)) to setup VPC network.
- We automatically generate the **Public Subnet x2**, **Private Subnet x2**.

### Step 2 : Setup Environment
#### Step 2.1 : Create Cloud9
- Go to [AWS Cloud9](https://console.aws.amazon.com/cloud9/home/product), click create environment
- Type the name you like.
- Let all setting **default**, then **Create environment**.

#### Step 2.2 : Upload Local Files
- Click **File** in the upper left corner, then click **Upload Local Files...**
- Click **Select folder**, then upload **[materials](./materials)**.

#### Step 2.3 : Install package & command
- Use command to install the package which includes `docker-compose`, `eksctl`, `kubectl` use the bash file.
    > First, we need to give the file permissions to **read, write, execute**.
    ```bash
    cd materials
    chmod 777 setup.sh
    ./setup.sh
    ```

#### Step 2.4 : Build & push image to ECR
- Using docker-compose to **create ECR**, then **build** and **push image** to ECR.
    ```bash
    chmod 777 run_docker.sh
    ./run_docker.sh
    ```
- After success, you will see [ECR](https://console.aws.amazon.com/ecr/repositories?region=us-east-1) has two repositories generated.

### Step 3 : Create Cluster
### Step 3.1 : Create IAM Role
- First, setup the **IAM role** for **ec2** to create **EKS cluster**.
    - Go to [AWS IAM Role](https://console.aws.amazon.com/iam/home?region=us-east-1#/roles), click create Role.
        <div align="center"><img src="./images/IAM_createRole_1.png" width="35%" height="35%"></div>
    - Select **EC2** as use case, then click **Next:Permissions**.
        <div align="center"><img src="./images/IAM_createRole_2.png" width="35%" height="35%"></div>
    - Click **Create Policy**, we need to create **EKS full access**.
        - <div align="center"><img src="./images/IAM_createRole_3.png" width="50%" height="50%"></div>
        - In **Service**, select **EKS**.
            <div align="center"><img src="./images/IAM_createPolicy_1.png" width="35%" height="35%"></div>
        - In **Action**, select **All EKS actions**.
            <div align="center"><img src="./images/IAM_createPolicy_2.png" width="35%" height="35%"></div>
        - In **Resource**, select **All resources**.
            <div align="center"><img src="./images/IAM_createPolicy_3.png" width="35%" height="35%"></div>
        - Click Next: Tags, then Next: Review.
        - In Name, type **EKSFullAccess**, then **Create policy**.
    - Go back to Create Role and **refresh**, search **EKS** you can find the policy **"EKSFullAccess"** below.
        <div align="center"><img src="./images/IAM_createRole_4.png" width="35%" height="35%"></div>
    - Click Next: Tags, then Next: Review.
    - In Name, type **EKSRole**, then **Create role**.

- After create role, sreach EKSRole and click in.
    - <div align="center"><img src="./images/IAM_createRole_5.png" width="35%" height="35%"></div>
    - You need to **attach other policy**, which include following policy:
        - <div align="center"><img src="./images/IAM_createRole_6.png" width="35%" height="35%"></div>
        - **AmazonEC2FullAccess**
        - **IAMFullAccess**
        - **AmazonSSMFullAccess**
        - **AWSCloudFormationFullAccess**
    - After that, you can find there are **five** policy in your role.
        <div align="center"><img src="./images/IAM_createRole_7.png" width="35%" height="35%"></div>

- Go to [EC2 Instances console](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:).
    - Find the instance we create through Cloud9, right-click and find **Security**, then click **Modify IAM role**.
    - In **Modify IAM role**, select **EKSRole**.

- Go to cloud9 IDE, copy-paste the following environment in command line area.
    > This environment stores the **vpc ID**, we will use it to create the cluster later.
    ```bash
    export VPC_NAME=EKS-VPC
    export PUBLIC_SUBNET_1_NAME=EKS-PublicSubnet01
    export PUBLIC_SUBNET_2_NAME=EKS-PublicSubnet02
    export PRIVATE_SUBNET_1_NAME=EKS-PrivateSubnet01
    export PRIVATE_SUBNET_2_NAME=EKS-PrivateSubnet02

    export ACCOUNT=$(aws sts get-caller-identity --query Account --output text)
    export VPC_ID=$(aws ec2 describe-vpcs --filter Name=tag:Name,Values=$VPC_NAME --query Vpcs[].VpcId --output text)
    export PUBLIC_SUBNET_1_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PUBLIC_SUBNET_1_NAME --query 'Subnets[?MapPublicIpOnLaunch==`true`].SubnetId' --output text)
    export PUBLIC_SUBNET_2_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PUBLIC_SUBNET_2_NAME --query 'Subnets[?MapPublicIpOnLaunch==`true`].SubnetId' --output text)
    export PRIVATE_SUBNET_1_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PRIVATE_SUBNET_1_NAME --query 'Subnets[?MapPublicIpOnLaunch==`false`].SubnetId' --output text)
    export PRIVATE_SUBNET_2_ID=$(aws ec2 describe-subnets --filter Name=tag:Name,Values=$PRIVATE_SUBNET_2_NAME --query 'Subnets[?MapPublicIpOnLaunch==`false`].SubnetId' --output text)
    ```

- we need to **close cloud9 credential** so that the role we just set can work.
    - Click the **icon** in the upper left corner, select **Preferences**.
        <div align="center"><img src="./images/cloud9_credential_1.png" width="35%" height="35%"></div>
    - Find the **AWS Setting** in the right panel, then **close "AWS managed temporary credentials"**.
        <div align="center"><img src="./images/cloud9_credential_2.png" width="50%" height="50%"></div>

- Create Cluster
    > It will take 20 minutes.
    ```bash
    cd EKS-config
    chmod 777 create_cluster.sh
    ./create_cluster.sh
    ```

- After Cluster Create, you can type the following cmd to check environment. <br>
    There will be an instance in public subnet and two instances in private subnet.
    ```
    kubectl get node -o wide
    ```
    <img src="./images/cluster_1.png" width="100%" height="100%">
- Also, there will have **three instances** in EC2 consloe.
    <img src="./images/cluster_2.png" width="80%" height="80%">

### Step 4 : Create Namespace
- Create Namespace **`k8s-demo`**
    > The namespace is like a **virtual cluster** that can help you **manage different applications** based on projects, groups, etc.
    ```
    kubectl apply -f namespace.yaml
    ```
- Check Namespace
    ```
    kubectl get namespace
    ```

### Step 5 : Create Pod through Deployment
- Update your **account ID** in deployment.yaml both **web-deployment** and **redis-deployment**.
    <img src="./images/deployment_1.png" width="60%" height="60%">
- Create Deployment
    ```
    kubectl apply -f deployment.yaml
    ```
- Check Pods
    ```
    kubectl get pods -n k8s-demo -o wide
    ```
    <img src="./images/deployment_2.png" width="100%" height="100%">

### Step 6 : Create Service
- Create Service
    ```
    kubectl apply -f service.yaml
    ```
- Check Service
    ```
    kubectl get service -n k8s-demo -o wide
    ```
    > You can see `web-service` Service Port(8000) is mapped to Node Port(30282).
    <img src="./images/service.png" width="100%" height="100%">

### Step 7 : Create Ingress
- Before create Ingress, we need to **setup ingress environment** using bash file.
    ```bash
    chmod 777 setup_ingress.sh
    ./setup_ingress.sh
    ```
- Create Ingress
    ```
    kubectl apply -f ingress.yaml
    ```
- Check Ingress
    ```
    kubectl get ingress -n k8s-demo
    ```
    <img src="./images/ingress_1.png" width="100%" height="100%">
- Also, there will have Load Balancer in EC2 consloe.
    <div align="center"><img src="./images/ingress_2.png" width="50%" height="50%"></div>
- Wait for **State** to becomce **active**, copy **DNS name**, add the path **"/home"**, and paste to browser. You will see your IP and the number of visits to the website. <br>
    <img src="./images/result.png" width="80%" height="80%">

## Clean up
- Delete EKS Cluster
```
eksctl delete cluster --region=us-east-1 --name=eks-cluster
```
- Delete following services:
    - ECR repository
    - Terminate EC2 created from Cloud9
    - IAM Role: "EKSRole"
    - VPC (subnet, route table, igw, nat gateway)

## Conclusion
Congratulations!! <br>
This is your first step in learning how to use EKS to build Kubernetes. <br>
You have learned how to **create Clusters** in specific vpc subnet and **generate EC2 instance as the Node**. <br>
Also, use **.yaml** to write Kubernetes configuration. <br>
Such as write deployment.yaml to generate pods. <br>
Write service.yaml to connect pod and pod. <br>
Finally, write ingress.yaml to auto-generate Application Load Balancer.

## Reference
[Kubernetes Documentation](https://kubernetes.io/docs/home/) <br>
[AWS EKS User Guide](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html) <br>
[Amazon EKS Workshop](https://www.eksworkshop.com/) <br>
[Kubernetes Distribution 元年: EKS Distro 想要說的事](https://medium.com/starbugs/kubernetes-distribution-%E5%85%83%E5%B9%B4-eks-distro-%E6%83%B3%E8%A6%81%E8%AA%AA%E7%9A%84%E4%BA%8B-b3fb732b5afe) <br>
[Kubernetes Application Deployment with AWS EKS and ECR](https://towardsdatascience.com/kubernetes-application-deployment-with-aws-eks-and-ecr-4600e11b2d3c) <br>
[Kubernetes 30天學習筆記](https://ithelp.ithome.com.tw/articles/10192401) <br>
[Kubernetes 基礎教學（一）原理介紹](https://cwhu.medium.com/kubernetes-basic-concept-tutorial-e033e3504ec0) <br>
[[Kubernetes] Cluster Architecture](https://godleon.github.io/blog/Kubernetes/k8s-CoreConcept-Cluster-Architecture/) <br>
[Kubernetes Service詳解（概念、原理、流量分析、代碼）](https://blog.csdn.net/liukuan73/article/details/82585732) <br>
[[Kubernetes] Service Overview](https://godleon.github.io/blog/Kubernetes/k8s-Service-Overview/) <br>
[Ingress Controllers](https://www.eksworkshop.com/beginner/130_exposing-service/ingress_controller_alb/) <br>